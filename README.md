# Nirex's Personal Coding Standard
A documentaion on all the coding standards from this date forward. (Previous Projects might get a refactor later)

# The What and The Why

This repo is made to host the MarkDown file that contains all the conventions 
and coding standards that I personally follow (And will require the people who
work on my projects to follow).

The coding standards written in the [NSTANDARD](NSTANDARD.md) are C++-centric, but the standards are to be followed no matter the language. 
A section in the [NSTANDARD](NSTANDARD.md) file may provide equivalent rules or exceptions for specific languages where it's necessary.

The reason for this standard are as follows: 

- 80% of the lifetime cost of a piece of software goes to maintenance.

- Hardly any software is maintained for its whole life by the original author.

- Code conventions improve the readability of the software, allowing engineers to understand new code more quickly and thoroughly.

# NOTE

This document is not in any way work in progress, it simply reflects the current coding standards in use by the owner of this repository: [Nirex](https://github.com/nirex0).

# CONTACT

Nirex.0@gmail.com
