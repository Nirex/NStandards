# Copyright Notice

Any source file (.h, .cpp, .cs, etc) must containe a copyright notice as the first line in the file. The format of the notice must exactly match one of the comments that shown below:

```cpp
// © <current year> <name> ALL RIGHTS RESERVED 
// © <project's starting year> - <current year> <name> ALL RIGHTS RESERVED 
```


##### Examples:

```cpp
// © 2018 NIREX ALL RIGHTS RESERVED 
// © 2015-2018 NIREX ALL RIGHTS RESERVED 
```

# Header file Safeguard Standard (.h)

All Header files must include at least one safeguard that follows this exact format

```cpp

#ifndef _<prefix>_<header file name>_H_ 
#define _<prefix>_<header file name>_H_ 
   
   // Code goes here 
   
#endif // !_<prefix>_<header file name>_H_ 
  
```

**Note:** Only Capital letter characters may be used in the safeguard preprocessor directives.

**Note:** Prefix must remain consistant in the entire project.

# Class Organization Standard (.h)

Classes should be organized with the order shown below:

**1.** public:

**2.** protected:

**3.** private:


**Note:** Only one ```public```, ```protected``` and ```private``` section may exist within a class

**C#:** All the public members of the class are to be written on top of all the protected members, followed by all the private members.

# Naming Conventions

* The first letter of each word in a name (such as type name or variable name) is capitalized, and there is usually no underscore between words. For example, Amount and NRegistry are correct, but not lastMouseCoordinates or delta_seconds.

* Type names are prefixed with an additional upper-case letter to distinguish them from variable names. For example, NCoords is a type name, and Coords is an instance of a NCoords.

* Template classes are prefixed by T.

* Enums are prefixed by E.

* Boolean variables must be prefixed by b (for example, bIsEnabled, or bHasLink).

* Type and variable names are nouns.

* Method names are verbs that describe the method's effect, or describe the return value of a method that has no effect.

* Variable, method, and class names should be clear, unambiguous, and descriptive. The greater the scope of the name, the greater the importance of a good, descriptive name. Avoid over-abbreviation.

* All variables should be declared one at a time, so that a comment on the meaning of the variable can be provided. Also, the JavaDocs style requires it. You can use multi-line or single line comments before a variable, and the blank line is optional for grouping variables.

* All functions that return a bool should ask a true/false question, such as IsVisible() or ShouldClearBuffer().

* A procedure (a function with no return value) should use a strong verb followed by an Object. An exception is, if the Object of the method is the Object it is in. Then the Object is understood from context. Names to avoid include those beginning with "Handle" and "Process" because the verbs are ambiguous.

* Though not required, it is best to prefix function parameter names with "Out" if they are passed by reference, and the function is expected to write to that value. This makes it obvious that the value passed in this argument will be replaced by the function.

* If an In or Out parameter is also a boolean, put the "b" before the In/Out prefix, such as bOutResult.

* Functions that return a value should describe the return value. The name should make clear what value the function will return. This is particularly important for boolean functions. Consider the following two example methods:

```cpp
bool CheckMouse(NRect Rect) {...} // what does true mean?
bool IsMouseWithin(NRect Rect) {...} // name makes it clear what true means.
```

##### Examples:

```cpp
float LinkWeight;
int32 LinkCount;
bool bDoesNodeExist;
std::string LinkID;
std::string LinkFriendlyName;
NCoords* LinkCoords;
```

# Comments

Comments are communication and communication is vital. The following sections 
detail some things to keep in mind about comments.

## Guidelines

* Write self-documenting code:
```cpp
// Bad:
t = h + i + o;

// Good:
TotalLayrs = HiddenLayers + InputLayers + OutputLayers;
```
 
* Write useful comments:
```cpp
// Bad:
// increment Items
++Items;

// Good:
// we know there is another Item
++Items;
```

* Do not comment bad code - rewrite it:
```cpp
// Bad:
// total number of nodes is sum of
// hidden, input and output layers
t = h + i + o;

// Good:
TotalLayrs = HiddenLayers + InputLayers + OutputLayers;
```

* Do not contradict the code:
```cpp
// Bad:
// never increment Nodes!
Nodes++;

// Good:
// we know there is another node
Nodes++;
```

# Const correctness

Const is documentation as much as it is a compiler directive, so all code should strive to be const-correct.

This includes:

* Passing function arguments by const pointer or reference if those arguments are not intended to be modified by the function,

* Flagging methods as const if they do not modify the object,

* and using const iteration over containers if the loop isn't intended to modify the container.

##### Example:

```cpp
// InArray will not be modified by SomeMutatingOperation, but OutResult probably will be
void SomeMutatingOperation(int& OutResult, const Array<int32>& InArray); 

void BAR::SomeNonMutatingOperation(void) const
{
    // This code will not modify the BAR it is invoked on
}

Array<std::string> StringArray;
for (const std::string& : StringArray)
{
    // The body of this loop will not modify StringArray
}
```

* One exception to this is pass-by-value parameters, which will ultimately be moved into a container (see "Move semantics"), but this should be rare.

##### Example:

```cpp
void SClass::SetMemberArray(Array<std::string> InNewArray)
{
    MemberArray = MoveTemp(InNewArray);
}
```

* Put the const keyword on the end when making a pointer itself const (rather than what it points to). References can't be "reassigned" anyway, and so can't be made const in the same way.

```cpp
// Const pointer to non-const object - pointer cannot be resassigned, but T can still be modified
T* const Ptr = ...;
// Illegal
T& const Ref = ...;
```

* Never use const on a return type, as this inhibits move semantics for complex types, and will give compile warnings for built-in types. This rule only applies to the return type itself, not the target type of a pointer or reference being returned.

``` cpp
// Bad - returning a const array
const Array<std::string> GetSomeArray();
// Fine - returning a reference to a const array
const Array<std::string>& GetSomeArray();
// Fine - returning a pointer to a const array
const Array<std::string>* GetSomeArray();
// Bad - returning a const pointer to a const array
const Array<std::string>* const GetSomeArray();
```

# C++11 Syntax

**static_assert**

This keyword is valid for use where you need a compile-time assertion.

**override** and **final**

These keywords are valid for use, and their use is strongly encouraged.

**nullptr**

nullptr should be used instead of the C-style NULL macro in all cases.

**auto**

You shouldn't use auto in C++ code, although a few exceptions are listed below. Always be explicit about the type you're initializing. This means that the type must be plainly visible to the reader. This rule also applies to the use of the var keyword in C#.

When is it acceptable to use auto?

* When you need to bind a lambda to a variable, as lambda types are not expressible in code.
* For iterator variables, but only where the iterator's type is verbose and would impair readability.
* In template code, where the type of an expression cannot easily be discerned. This is an advanced case.

It's very important that types are clearly visible to someone who is reading the code. Even though some IDEs are able to infer the type, doing so relies on the code being in a compilable state. It also won't assist users of merge/diff tools, or when viewing individual source files in isolation, such as on GitHub.

If you're sure you are using auto in an acceptable way, always remember to correctly use const, & or * just like you would with the type name. With auto, this will coerce the inferred type to be what you want.

**Range Based For**

This is preferred to keep the code easier to understand and more maintainable. 

##### Example:

```cpp
for (Object* Property : ObjectArray)
{
     // Iterate through the entire ObjectArray and access each value through Property variable 
}
```

**Lambdas and Anonymous Functions**

Lambdas can now be used on all compilers, but their usage should be considered. The best lambdas should be no more than a couple of statements in length, particularly when used as part of a larger expression or statement, for example as a predicate in a generic algorithm

Be aware that stateful lambdas can't be assigned to function pointers, which we tend to use a lot.

You should consider documentation of non-trivial lambdas and anonymous functions the same way you document a regular function. Don't be afraid to split them over a few more lines in order to include comments.

It's better to use explicit capture over automatic capture ([&] and [=]), especially for large lambdas and deferred execution. Accidentally capturing a variable with the wrong capture semantics can have negative consequences, and this is more likely to happen as the code is maintained over time.

* By-reference capture and by-value capture of pointers can cause accidental dangling references, if the lambda is executed outside the context of the captured variables:

```cpp
void Func()
{
    int32 Value = GetSomeValue();
    // Lots of code
    AsyncTask([&]()
    {
        // Value is invalid here
        for (int Index = 0; Index != Value; ++Index)
        {
            // ...
        }
    });
}
```
* By-value capture can be a performance concern because of unnecessary copies:

```cpp
void Func()
{
    int32 ValueToFind = GetValueToFind();
    // The lambda takes a copy of ArrayOfThings because it is accidentally captured by [=] when it was only meant to capture ValueToFind
    CThing* Found = ArrayOfThings.FindByPredicate
    (
        [=](const CThing& Thing)
        {
            return Thing.Value == ValueToFind && Thing.Index < ArrayOfThings.Num();
        }
    );
}
```

* Automatic capture always captures "this" implicitly if any member variables are referenced, even with [=]. [=] gives the impression of the lambda having its own copy of the member when it doesn't:

```cpp
void XStruct::Func()
{
    int32 Local = 5;
    Member = 5;
    auto Lambda = [=]()
    {
      SomeLoggerFunction(Local, Member);
    };
    Local = 100;
    Member = 100;
    Lambda();
}
```

It's better to use explicit return types for large lambdas, or when you are returning the result of another function call. These should be considered in the same way as the auto keyword:

```cpp
// Without the return type here, the return type is unclear
auto Lambda = []() -> MyType
{
    return SomeFunc();
}
```

Automatic captures and implicit return types are acceptable for trivial lambdas, such as in Sort calls, where the semantics are obvious and being explicit would make it overly verbose. Use your best judgment.

**Strongly-Typed Enums**

Enum classes should always be used as a replacement for old-style namespaced enums. For example:

```cpp
// Old enum
namespace EThing
{
    enum Type
    {
        Thing1,
        Thing2
    };
}
// New enum
enum class EThing : uint8
{
    Thing1,
    Thing2
}
```

# Code Formatting

**Braces {}**

Always include braces in single-statement blocks. For example.:

```cpp
if (bThing)
{
    return;
}
```

**If - Else**

Each block of execution in an if-else statement should be in braces. This is to prevent editing mistakes - when braces are not used, someone could unwittingly add another line to an if block. The extra line wouldn't be controlled by the if expression, which would be bad. It's also bad when conditionally compiled items cause if/else statements to break. So always use braces.

```cpp
if (bIsEverythingAlright)
{
    Answer("Yes");
}
else
{
    Answer("No!");
}
```

A multi-way if statement should be indented with each else if indented the same amount as the first if; this makes the structure clear to a reader:

```cpp
if (Weight < 10)
{
    Log("Low Weight");
}
else if (TannicAcid < 100)
{
    Log("Medium Weight");
}
else
{
    Log("High Weight");
}
```

**Tabs and Indenting**

Here are the standards for indenting your code.

* Indent code by execution block.

* Use tabs, not spaces, for whitespace at the beginning of a line. Set your tab size to 4 characters. However, spaces are sometimes necessary and allowed for keeping code aligned regardless of the number of spaces in a tab. For example, when you are aligning code that follows non-tab characters.

* If you are writing code in C#, please also use tabs, and not spaces. The reason for this is that programmers often switch between C# and C++, and most prefer to use a consistent setting for tabs. Visual Studio defaults to using spaces for C# files, so you will need to remember to change this setting when working on a C# project.

**Switch Statements**

Except for empty cases (multiple cases having identical code), switch case statements should explicitly label that a case falls through to the next case. Either include a break, or include a falls-through comment in each case. Other code control-transfer commands (return, continue, and so on) are fine as well.

Always have a default case, and include a break just in case someone adds a new case after the default.

```cpp
switch (condition)
{
    case 1:
        ...
        // falls through
    case 2:
        ...
        break;
    case 3:
        ...
        return;
    case 4:
        ...
        continue;
    case 5:
        ...
        break;
    default:
        break;
}
```

# Namespaces

You can use namespaces to organize your classes, functions and variables where appropriate. If you do use them, follow the rules below.

* Using declarations:
  - Do not put *using* declarations in the global scope, even in a .cpp file.
  - It's okay to put *using* declarations within another namespace, or within a function body.
  - If you put *using* declarations within a namespace, this will carry over to other occurrences of that namespace in the same translation unit. As long as you are consistent, it will be fine.
  - You can only use *using* declarations in header files safely if you follow the above rules.
  
* Note that forward-declared types need to be declared within their respective namespace. If you don't do this, you will get link errors.

* If you declare a lot of classes/types within a namespace, it can be difficult to use those types in other global-scoped classes (for example, function signatures will need to use explicit namespace when appearing in class declarations).

* You can use *using* declarations to only alias specific variables within a namespace into your scope (for example, using Foo::Bar). However, that is quite uncommon.

# Encapsulation

* Enforce encapsulation with the protection keywords. Class members should almost always be declared private unless they are part of the public/protected interface to the class. Use your best judgement, but always be aware that a lack of accessors makes it hard to refactor later without breaking plugins and existing projects.

* If particular fields are only intended to be usable by derived classes, make them private and provide protected accessors.

* Use *final* if your class is not designed to be derived from.

# General Style Issues

* Minimize dependency distance. When code depends on a variable having a certain value, try to set that variable's value right before using it. Initializing a variable at the top of an execution block, and not using it for a hundred lines of code, gives lots of space for someone to accidentally change the value without realizing the dependency. Having it on the next line makes it clear why the variable is initialized the way it is and where it is used.

* Split methods into sub-methods where possible. It is easier for someone to look at a big picture, and then drill down to the interesting details, than it is to start with the details and reconstruct the big picture from them. In the same way, it is easier to understand a simple method, that calls a sequence of several well-named sub-methods, than it is to understand an equivalent method that simply contains all the code in those sub-methods.

* In function declarations or function call sites, do not add a space between the function's name and the parentheses that precede the argument list.

* Address compiler warnings. Compiler warning messages mean something is wrong. Fix what the compiler is warning you about. If you absolutely can't address it, use #pragma to suppress the warning, but this should only be done as a last resort.

* Leave a blank line at the end of the file. All .cpp and .h files should include a blank line, to coordinate with gcc.

* Interface classes (prefixed with "I") should always be abstract, and must not have member variables. Interfaces are allowed to contain methods that are not pure-virtual, and can even contain methods that are non-virtual or static, as long as they are implemented inline.

* Debug code should either be generally useful and polished, or not checked in. Debug code that is intermixed with other code makes the other code harder to read.

* Avoid repeating the same operation redundantly in loops. Move common subexpressions out of loops to avoid redundant calculations. Make use of statics in some cases, to avoid globally-redundant operations across function calls.

* Be mindful of hot reload. Minimize dependencies to cut down on iteration time. Don't use inlining or templates for functions which are likely to change over a reload. Only use statics for things which are expected to remain constant over a reload.

* Use intermediate variables to simplify complicated expressions. If you have a complicated expression, it can be easier to understand if you split it into sub-expressions, that are assigned to intermediate variables, with names describing the meaning of the sub-expression within the parent expression. For example:

```cpp
if ((Foo->Bar->Property->Etc && OtherStuff) &&
   !(bNodeExists && bIsTraining && bNoError &&
   IsTuesday())))
{
    DoSomething();
}
```
*should be replaced with*
```cpp
const bool bCondition0 = Foo->Bar->Property->Etc && OtherStuff;
const bool bCondition1 = bNodeExists && bIsTraining && bNoError && IsTuesday();
if (bCondition0 && !bCondition1)
{
    DoSomething();
}
```

* Use the virtual and override keywords when declaring an overriding method. When declaring a virtual function in a derived class, that overrides a virtual function in the parent class, you must use both the virtual and the override keywords. For example:

```cpp
class A
{
public:
    virtual void F() {}
};
class B : public A
{
public:
    virtual void F() override;
}
```

* Pointers and references should only have one space, which is to the right of the pointer or reference. This makes it easy to quickly use Find in Files for all pointers or references to a certain type.

*Use this:*
```cpp
ClassType* Type;
```
*Not these:*
```cpp
ClassType * Type0;
ClassType *Type1;
```

* Shadowed variables are not allowed. C++ allows variables to be shadowed from an outer scope, but this makes usage ambiguous to a reader. For example, there are three usable Count variables in this member function:

```cpp
class SomeClass
{
public:
    void Func(const int32 Count)
    {
        for (int32 Count = 0; Count != 10; ++Count)
        {
            // Use Count
        }
    }
private:
    int32 Count;
}
```
